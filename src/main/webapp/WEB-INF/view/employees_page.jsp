<!DOCTYPE html>
<%@ page session="false"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="epam.edu.bean.*, java.util.*"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false"%>

<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<style>
.error {
	color: red;
	font-weight: bold;
}
</style>

		<h2>List Of employees</h2>
		<form action="${pageContext.request.contextPath}/add_employee"
			method="get">
			<input type="submit" name="button1" value="new employee"
				class="btn btn-primary" />
		</form>
<br>
<body>

	<table title="List Of employees" class="table table-striped">

		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Email</th>
			<th>Birthdate</th>
			<th>Manager</th>
			<th>Primary_Skill</th>
			<th>Level</th>
			<th>Action</th>
		</tr>

		<c:forEach items="${employees}" var="element">

			<tr>
				<td>${element.id}</td>
				<td>${element.name}</td>
				<td>${element.email}</td>
				<td>${element.birthdate}</td>
				<td>${element.manager}</td>
				<td>${element.skill}</td>
				<td>${element.level}</td>

				<td><a href="<c:url value='/delete/${element.id}' />"
					class="btn btn-danger custom-width">delete</a> <a
					href="<c:url value='/update/${element.id}' />"
					class="btn btn-danger custom-width">update</a></td>
			</tr>
		</c:forEach>

	</table>

</body>
