<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page session="false"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="epam.edu.bean.*, java.util.*"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table  class="table table-striped">
		<tr>
			<th>Name</th>
			<th>Mentor</th>
			<th>office location</th>
		</tr>
		<c:forEach items="${statistic5}" var="statistic">

			<tr>
				<td>${statistic[0]}</td>
				<td>${statistic[1]}</td>
				<td>${statistic[2]}</td>
			</tr>
		</c:forEach>
		</table>
</body>
</html>