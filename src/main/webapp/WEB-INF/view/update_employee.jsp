
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
	$.noConflict(); 
	jQuery(document).ready(function($) {
		$("#birthdate").datepicker({
			dateFormat : "yy-mm-dd",
			changeMonth : true,
			changeYear : true,
			maxDate : "+0D"
		});
	});
</script>
<style>
.error {
	color: red;
	font-weight: bold;
}
</style>

</head>
<body>
	<h3>Update employee</h3>
	<form:form commandName="employee" method="POST"
		action="/mp/update_employee" modelAttribute="employee">
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="name">Enter name</label>
				<div class="col-md-7">
					<form:input type="text" path="name" id="name" value="${employee.name}"
						class="form-control input-sm" required="required" />
					<div class="has-error">
						<form:errors path="name" class="help-inline" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="email">Email</label>
				<div class="col-md-7">
					<form:input type="text" path="email" id="email" value="${employee.email}"
						class="form-control input-sm" required="required"
						placeholder="Enter email" />
					<div class="has-error">
						<form:errors path="email" class="error" align="left" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="email">Birthdate</label>
				<div class="col-md-7">
					<form:input type="text" path="birthdate" id="birthdate"
						placeholder="choose birthdate"  
						class="form-control input-sm" value="${employee.birthdate}" />
					<%-- <div class="has-error">
						<form:errors path="birthdate" class="error" align="left" />
					</div> --%>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="manager">Manager</label>
				<div class="col-md-7">
					<form:input type="text" path="manager" id="manager"
						class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="manager" class="error" align="left" />
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="skill">Primary
					skill</label>
				<div class="col-md-7">
					<form:select class="form-control input-sm" path="skill"
						classs="form-control input-sm">
						<form:option value="NONE" label="--- Select ---" items="${skills}" />
						<form:options />
					</form:select>
					<div class="has-error">
						<form:errors path="skill" class="help-inline" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="level">Primary
					skill</label>
				<div class="col-md-7">
					<form:radiobuttons class="btn btn-primary btn-sm" path="level"
						level="${levels}" id="skill" name="skill"
						classs="form-control input-sm" />

					<div class="has-error">
						<form:errors path="skill" class="help-inline" />
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-12">
				<div class="col-md-7">
					<input type="submit" value="Save" class="btn btn-primary btn-sm" />
					or <a href='${pageContext.request.contextPath}/employees'>Cancel</a>
				</div>
			</div>
		</div>

	<input type="hidden" id="id" name="id" value="${employee.id}" />
	<br />
	<input type="hidden" id="createdBy" name="createdBy"
		value="${employee.createdBy}" />
	<br />
	<input type="hidden" id="dateCreated" name="dateCreated"
		value="${employee.dateCreated}" />
	<br />
	</form:form>
</body>
</html>