
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@page import="epam.edu.bean.*, java.util.*"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false"%>
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>
.error {
	color: red;
	font-weight: bold;
}
</style>

<br>

<div>
	<h2>List Of Groups</h2>
	<table title="List Of Groups" class="table table-striped">
		<tr>
			<th>id</th>
			<th>mentor</th>
			<th>mentee</th>
			<th>planned start</th>
			<th>planned end</th>
			<th>actual start</th>
			<th>actual end</th>
			<th>status</th>
		</tr>

		<c:forEach items="${groups}" var="element">
		
			<tr>
				<td>${element.id}</td>
				<td>${element.mentor.name}</td>
				<td>${element.mentee.name}</td>
				<td>${element.plannedStart}</td>
				<td>${element.plannedEnd}</td>
				<td>${element.actualStart}</td>
				<td>${element.actualEnd}</td>
				<td>${element.status}</td>
			</tr>
		</c:forEach>

	</table>

</div>

