<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
		<h2>Practical tasks</h2>
		<div class="col-sm-8 text-left">
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapse1">Task 1 - Spring MVC</a>
						</h4>
					</div>
					<div id="collapse1" class="panel-collapse collapse in">
						<div class="panel-body">
							<ol>
								<li>Create web application with configured Spring MVC
									dispatcher servlet and application context.<br>
								<li>Create model beans and DAO layer for users and
									mentorship programs (make it as simple as possible, can be
									in-memory Map<Id , Bean> with sample data).<br>
									<li>Implement service layer for operating with the model
										above.<br>
									<li>Add controllers capable of creating domain objects,
										editing, getting by id, deleting and searching by simple
										criteria.<br>
									<li>Add View resolver and write simple front-end pages
										(JSP or FreeMarker or Velocity) to connect to controller
										logic.
							</ol>
							<div class="well well-sm">
								<h3>Criteria</h3>
							</div>
							<ul>
								<li>Your controllers should provide responses of various
									types: HTML pages and object-based (like JSON or XML)
								<li>Custom formatter for Date fields should be configured
								<li>Saving and editing must be accompanied with validation
									logic
								<li>Exception handler should be configured and redirect to
									error page or return proper error message depending on
									controller type
								<li>Remember about S.O.L.I.D. principles
							</ul>
							<div class="well well-sm">
								<h3>Tips</h3>
							</div>
							Task assumes certain knowledge of Servlet API and Spring Core:
							you might need to review webinars and browse through some
							examples.
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapse2">Task 2 - Spring AOP</a>
						</h4>
					</div>
					<div id="collapse2" class="panel-collapse collapse">
						<div class="panel-body">
							<ol>
								<li>Add next fields to main model beans:
									<ol type="a">
										<li>Date created
										<li>Created by User
										<li>Date last modified
										<li>Last modified by User
									</ol>
								<li>Write logic that sets proper values for these fields
									after relevant user operations.
								<li>Write logic to log all DAO calls with as many details
									as possible; use additional logging file for calls ended with
									an exception.
							</ol>
							<div class="well well-sm">
								<h3>Criteria</h3>
							</div>
							<ul>
								<li>Several types of pointcut designators should be used
									(bound to method names, method arguments, annotations etc.)
								<li>Several advice types should be used (before, after
									throwing, around etc.)
							</ul>
							<div class="well well-sm">
								<h3>Q/A</h3>
							</div>
							Should I implement some kind of authentication mechanism to
							fill-in fields "Created by User" and "Last modified by User"? <br>
							A: You might if you like, though "Spring Security" is not in
							lectures' scope, so a simple approach would be enough. For
							example, using caller's IP address.
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapse3">Task 3 - JPA/JPQL</a>
						</h4>
					</div>
					<div id="collapse3" class="panel-collapse collapse">
						<div class="panel-body">
							<ol>
								<li>Design a DB scheme corresponding to your domain model
									(people, mentorship programs, phase participant assignments,
									groups).
								<li>Create object-relational mapping from your Java beans
									to DB tables using JPA.
								<li>Configure one of JPA implementations and leverage it
									from your DAO layer.
								<li>Except normal add / edit / remove operations, there
									should be mehods to:
									<ol type="a">
										<li>Get list of mentors who mentors more than 2 mentees
											at this moment (only active).
										<li>Get list of mentees who are currently without mentors
											in user-selected location.
										<li>Get list of all mentees and duration of their overall
											mentorship (counted in mentor-weeks) ordered descending with
											pagination.
										<li>Get statistic of how many programs currently exist
											within each city and how many people are involved into them.
										<li>Get statistic of success completions (e.g. % of
											people who completed the whole phase and not ended up in the
											middle) per user-selected period of time.
									</ol>
							</ol>
							<div class="well well-sm">
								<h3>Criteria</h3>
							</div>
							<ul>
								<li>Fields of various type should be used: primitives,
									date, enumerations, collections of primitives, binary data
									(like avatar), booleans.
								<li>Cascading should be properly configured for relations.
								<li>Both JPQL and Criteria API should be used.
								<li>Performance should be optimized: use lazy load
									configuration, cashing, entity graphs etc.
							</ul>
							<div class="well well-sm">
								<h3>Tips</h3>
							</div>
							You may use code generation to avoid creating DB by SQL scripts.
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapse4">Task 4 - JMS</a>
						</h4>
					</div>
					<div id="collapse4" class="panel-collapse collapse">
						<div class="panel-body">
							<ol>
								<li>Implement simple authentication mechanism in your
									application (can be HTTP basic form login and logout).
								<li>Setup a messaging system (RabbitMQ, ActiveMQ or other
									of your choice).
								<li>Create destination and implement tracking of user
									activities:
									<ul>
										<li>login success
										<li>login failure
										<li>logout (or session timeout).
									</ul>
								<li>Create destination and implement tracking of activity
									details:
									<ul>
										<li>page loading (e.g. requests to view / add / edit /
											delete main elements) including free-form additional data as
											JSON or else.</li>
									</ul>
								<li>Create Consumer application to:
									<ol type="a">
										<li>save log-in/log-out information into a DB [Criteria:
											same DB];
										<li>build statistic of spent time (logged-in time) by
											users [Criteria: export statistic into XLS monthly or on
											demand];
										<li>sent warning email to Administrator if within 10
											minutes:
											<ul>
												<li>login for the same user failed 3 times (e.g. same
													login/name);
												<li>total login failures exceeds 10 (e.g. same IP, but
													different login/name).
											</ul>
									</ol>
								<li>save page opening logs into DB [Criteria: separate DB].
							</ol>
							<div class="well well-sm">
								<h3>Criteria</h3>
							</div>
							<ul>
								<li>Use JMS selection filters feature (e.g. failed logins);
								<li>Use JMS message prioritization feature (e.g. admin
									activities);
								<li>Leverage JMS durable subscription and message
									acknowledgement (e.g. failed logins).
							</ul>
							<div class="well well-sm">
								<h3>Tips</h3>
							</div>
							Consumer can be a module of your main application and not
							separate JVM - for simplicity. In-memory cache can be used for
							5c.
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapse5">Task 5 - GIT</a>
						</h4>
					</div>
					<div id="collapse5" class="panel-collapse collapse">
						<div class="panel-body">

							<ul>
								<li>ensure you're using GIT for your application
									development;
								<li>leverage proper workflow & branching mechanism
									(separate branch per task/topic at least).
							</ul>
							<div class="well well-sm">
								<h3>Deliverables</h3>
							</div>
							console logs of GIT CLI and/or screenshots from GIT UI showing
							usage of all commands listed in "Basic Commands and their
							specifics" agenda point.
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapse6">Task 6 - Spring Boot</a>
						</h4>
					</div>
					<div id="collapse6" class="panel-collapse collapse">
						<div class="panel-body">Make your application configuration
							SpringBoot-based, optimize and refactor Java code respectively.</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapse7">Task 7 - XML </a>
						</h4>
					</div>
					<div id="collapse7" class="panel-collapse collapse">
						<div class="panel-body">
							<ul>
								<li>Implement parses (DOM/SAX/StAX) that will parse <a
									href="https://kb.epam.com/download/attachments/359322840/XML_TASK.xml?version=1&modificationDate=1478094726000&api=v2">
										attached file </a>
								<li>Use DOM parser in combination with XPath get all books
									where second name is O'Brien
								<li>Use XQuery to find all books that have price bigger
									then 5.95
								<li>Use JaxB to parse attached xml file
							</ul>
							<div class="well well-sm">
								<h3>Deliverables</h3>
							</div>
							console logs of GIT CLI and/or screenshots from GIT UI showing
							usage of all commands listed in "Basic Commands and their
							specifics" agenda point.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

