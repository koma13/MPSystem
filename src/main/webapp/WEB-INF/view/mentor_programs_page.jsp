
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@page import="epam.edu.bean.*, java.util.*"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false"%>
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>
.error {
	color: red;
	font-weight: bold;
}
</style>
<div>
	<h2>List Of Mentorship programs</h2>
	<table title="List Of Mentorship programs" class="table table-striped">
		<tr>
			<th>Name</th>
			<th>officeLocation</th>
			<th>startDate</th>
			<th>endDate</th>
		</tr>

		<c:forEach items="${programs}" var="element">

			<tr>
				<td>${element.name}</td>
				<td>${element.officeLocation}</td>
				<td>${element.startDate}</td>
				<td>${element.endDate}</td>
			</tr>
		</c:forEach>

	</table>

</div>

