<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="ce" %>
<html>
<head>
<meta name="_csrf_parameter" content="_csrf" />
<meta name="_csrf_header" content="X-CSRF-TOKEN" />
<meta name="_csrf" content="e62835df-f1a0-49ea-bce7-bf96f998119c" />
	<link rel="stylesheet" type="text/css" href="${url}/resources/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="${url}/resources/css/springmvc.css"/>
	<script src="${url}/resources/js/jquery.min.js"></script>
	<script src="${url}/resources/js/bootstrap.min.js"></script>	
</head>
<style>

.error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}

input[type=text], input[type=password] {
	width: 100%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

button {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 100%;
}

span.psw {
	float: right;
	padding-top: 16px;
}

@media screen and (max-width: 300px) {
	span.psw {
		display: block;
		float: none;
	}
}
</style>
<body onload='document.loginForm.username.focus();'>

	<h2>Login form</h2>

	<ce:if test="${not empty error}">
			<div class="error">${error}</div>
		</ce:if>
		<ce:if test="${not empty msg}">
			<div class="msg">${msg}</div>
		</ce:if>
	<c:url value="j_spring_security_check" var="loginUrl" />
	<form name='loginForm' action="${loginUrl}" method='POST'>
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		<div class="col-sm-10">
			<label><b>Username</b></label> <input type="text"
				placeholder="Enter Username" name='username' value='' required>
			<label> <b>Password</b></label> <input type="password"
				placeholder="Enter Password" name='password' required>

			<button type="submit" name="submit" value="submit">Login</button>
			<input type="checkbox" checked="checked"> Remember me
		</div>
	</form>
</body>
</html>

