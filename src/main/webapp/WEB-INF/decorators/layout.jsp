<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="dec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="url" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="${url}/resources/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="${url}/resources/css/springmvc.css"/>
	<script src="${url}/resources/js/jquery.min.js"></script>
	<script src="${url}/resources/js/bootstrap.min.js"></script>	
	<title>
		<dec:title default="Web Page" />
	</title>
	<dec:head />
</head>
<body>
<div class="container bs-docs-container">
    <div id="m_header">            
    	<%@ include file="/WEB-INF/includes/header.jsp"%>  
    </div>        		    
    <div id="m_menu_top">            
    	<%@ include file="/WEB-INF/includes/navigation.jsp"%>  
    </div>
    <div id="m_container">
			<div id="content">
	 		<div class="row">
	 			<div class="col-sm-10">
	 				<dec:body />
		   		</div>
		   		<div class="col-sm-2" style="border-left: 1px solid #fff">
		   			<%@ include file="/WEB-INF/includes/navigationLeft.jsp"%>
		   		</div>
	 		</div>	 	   
		</div>        
    </div>
</div>    
</body>
</html>