<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Bootstrap Case</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
	<nav class="navbar navbar-default">
	<div class="container-fluid">

		<ul class="nav navbar-nav" bs-active-link>
			<li><a href='${pageContext.request.contextPath}'>Home</a></li>
			<li><a href="employees">Employee</a></li>
			<li><a href="groups">Groups</a></li>
			<li><a href="pAssignment">Participant Assignments </a></li>
			<li><a href="programs">Mentorship Programs</a></li>
			<li><a href="lectures">Lectures</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="user"><span class="glyphicon glyphicon-log-in"></span>
					Login</a></li>
		</ul>
	</div>
	</nav>

</body>
</html>