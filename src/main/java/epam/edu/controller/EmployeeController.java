package epam.edu.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import epam.edu.bean.Employee;
import epam.edu.service.ActivityDetailsQueueService;
import epam.edu.service.EmployeeService;

@Controller
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private ActivityDetailsQueueService activityDetailsQueueService;

	@RequestMapping(value = "/employees")
	public ModelAndView showUserPage(ModelAndView model, HttpServletRequest request) {
		List<Employee> employees = employeeService.fetchemployees();
		model = new ModelAndView("employees_page");
		model.addObject("employees", employees);
		activityDetailsQueueService.addActivityDetailsQueue(request.getRemoteAddr(), "'view page' activity");
		return model;
	}

	@RequestMapping(value = "/add_employee", method = RequestMethod.GET)
	public ModelAndView showAddUserForm(ModelAndView model) {
		model = new ModelAndView("add_employee");
		model.addObject("levels", Employee.LEVEL.values());
		model.addObject("skills", Employee.PRIMARY_SKILL.values());
		model.addObject("employee", new Employee());

		return model;

	}

	@RequestMapping(value = "/add_employee", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("employee") @Valid Employee employee, BindingResult result, HttpServletRequest request, ModelAndView model) {
		if (!result.hasErrors()) {
			employeeService.add(employee);
			List<Employee> employees = employeeService.fetchemployees();
			model = new ModelAndView("employees_page");
			model.addObject("employees", employees);
			activityDetailsQueueService.addActivityDetailsQueue(request.getRemoteAddr(), "add " + employee.toStringShort());
		}
		return model;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public ModelAndView showUpdateForm(@PathVariable("id") Long id, ModelAndView model, HttpServletRequest request) {
		Employee employee = employeeService.fetchById(id);
		model = new ModelAndView("update_employee");
		model.addObject("employee", employee);
		model.addObject("levels", Employee.LEVEL.values());
		model.addObject("skills", Employee.PRIMARY_SKILL.values());
		return model;
	}

	@RequestMapping(value = "/update_employee", method = RequestMethod.POST)
	public ModelAndView update(@ModelAttribute("employee") @Valid Employee employee, BindingResult result, ModelAndView model, HttpServletRequest request) {
		if (!result.hasErrors()) {
			employeeService.update(employee);
			List<Employee> employees = employeeService.fetchemployees();
			model = new ModelAndView("employees_page");
			model.addObject("employees", employees);
			activityDetailsQueueService.addActivityDetailsQueue( request.getRemoteAddr(), "update employee activity " + employee.toStringShort());
		}
		return model;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
		employeeService.delete(id);
		List<Employee> employees = employeeService.fetchemployees();
		model.addAttribute("employees", employees);
		activityDetailsQueueService.addActivityDetailsQueue(request.getRemoteAddr(), "delete employee with id " + id);
		return "employees_page";

	}

	@InitBinder
	public void initBinder(final WebDataBinder binder) {
		final SimpleDateFormat dateFormat = new SimpleDateFormat("yy-mm-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
