package epam.edu.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import epam.edu.bean.Group;
import epam.edu.service.GroupService;

@Controller
public class GroupController {

	@Autowired
	private GroupService groupService;

	@RequestMapping(value = "/groups")
	public String showGroupPage(Model model) {
		List<Group> groups = groupService.fetchAll();
		model.addAttribute("groups", groups);
		return "groups_page";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/addGroup")
	public String addGroup(@ModelAttribute("group") @Valid Group group, BindingResult result, Model model) {
		if (!result.hasErrors()) {
			groupService.addAll(group);
		}
		List<Group> groups = groupService.fetchAll();
		model.addAttribute("groups", groups);
		return "groups_page";
	}
}
