package epam.edu.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import epam.edu.service.LoginDetailsQueueService;

@Controller
public class LoginController {

	@Autowired
	LoginDetailsQueueService loginDetailsQueueService;

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ModelAndView welcomePage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		return model;

	}

	@RequestMapping(value = "/user**", method = RequestMethod.GET)
	public ModelAndView adminPage(HttpServletRequest request, HttpServletResponse response) {
		String loginMsg = "User have been logged in successfully"; //TODO add user roles?
		loginDetailsQueueService.addLoginDetailsQueue(loginMsg, true, request.getRemoteAddr());
		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security");
		model.addObject("message", "This is protected page!Only for logged in users");
		model.setViewName("user");

		return model;

	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "logout", required = false) String logout) {
		String loginMsg = null;
		ModelAndView model = new ModelAndView();
		if (logout != null) {
			loginMsg = "User have been logged out successfully.";
			model.addObject("msg", loginMsg);
		}
		model.setViewName("login");
		return model;

	}

	@RequestMapping(value = "/loginError", method = RequestMethod.GET)
	public ModelAndView loginError(@RequestParam(value = "error", required = false) String error) {
		String loginMsg = null;
		ModelAndView model = new ModelAndView();
		loginMsg = "Invalid username and password!";
		model.addObject("error", loginMsg);
		model.setViewName("login");

		return model;

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
			loginDetailsQueueService.addLoginDetailsQueue(auth.getName(), "Logged out successfully", true, request.getRemoteAddr());
		}
		return "redirect:/login?logout";
	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject("username", userDetail.getUsername());
		}
		model.setViewName("403");
		return model;

	}

}
