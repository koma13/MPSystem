package epam.edu.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import epam.edu.bean.MentorshipProgram;
import epam.edu.service.MentorshipProgramService;

@Controller
public class MentorshipProgramController {
	
	@Autowired
	private  MentorshipProgramService mentorshipProgramService;
	
	@RequestMapping(value = "/programs")
	public String showUserPage(Model model) {
		List<MentorshipProgram> programs = mentorshipProgramService.fetchPrograms();
		model.addAttribute("programs", programs);
		return "mentor_programs_page";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/addProgram")
	public String addProgram(@ModelAttribute("program") @Valid MentorshipProgram program, BindingResult result, Model model) {
		if (!result.hasErrors()) {
			mentorshipProgramService.addProgram(program);
		}
		List<MentorshipProgram> programs = mentorshipProgramService.fetchPrograms();
		model.addAttribute("programs", programs);
		return "mentor_programs_page";
	}


}
