package epam.edu.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import epam.edu.bean.Lecture;
import epam.edu.service.LectureService;

@Controller
public class LectureController {

	@Autowired
	private LectureService lectureService;

	@RequestMapping(value = "/lectures")
	public String showLecturesPage(Model model) {
		List<Lecture> lectures =lectureService.fetchAll();
		model.addAttribute("lectures", lectures);
		return "lectures_page";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/addLecture")
	public String addGroup(@ModelAttribute("lecture") @Valid Lecture lecture, BindingResult result, Model model) {
		if (!result.hasErrors()) {
			lectureService.add(lecture);
		}
		List<Lecture> lectures = lectureService.fetchAll();
		model.addAttribute("lectures", lectures);
		return "lectures_page";
	}
}
