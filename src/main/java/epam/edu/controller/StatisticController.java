package epam.edu.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import epam.edu.service.StatisticService;

@Controller
public class StatisticController {

	@Autowired
	private StatisticService statisticService;

	@RequestMapping(value = "/statistic")
	public ModelAndView showAllStatistic(ModelAndView model) {
		statisticService.statistic1();
		model = new ModelAndView("statistic_page");
		return model;
	}

	@RequestMapping(value = "/statistic1")
	public ModelAndView statistic1(ModelAndView model) {
		List<Object[]> statistic1 = statisticService.statistic1();
		model.addObject("statistic1", statistic1);
		return model;
	}

	@RequestMapping(value = "/statistic2")
	public ModelAndView statistic2(ModelAndView model) {
		List<Object[]> statistic2 = statisticService.statistic2("Lviv");
		model.addObject("statistic2", statistic2);
		return model;
	}

	@RequestMapping(value = "/statistic3")
	public ModelAndView statistic3(ModelAndView model) {
		List<Object[]> statistic3 = statisticService.statistic3();
		model.addObject("statistic3", statistic3);
		return model;
	}

	@RequestMapping(value = "/statistic4")
	public ModelAndView statistic4(ModelAndView model) {
		List<Object[]> statistic4 = statisticService.statistic4();
		model.addObject("statistic4", statistic4);
		return model;
	}

	@RequestMapping(value = "/statistic5")
	public ModelAndView statistic5(ModelAndView model) {
		Date date = new Date();
		List<Object[]> statistic5 = statisticService.statistic5(date);
		model.addObject("statistic5", statistic5);
		return model;
	}

}
