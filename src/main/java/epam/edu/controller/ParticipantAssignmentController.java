package epam.edu.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import epam.edu.bean.ParticipantAssignment;
import epam.edu.service.ParticipantAssignmentService;

@Controller
public class ParticipantAssignmentController {
	
	@Autowired
	private ParticipantAssignmentService participantAssignmentService;



	@RequestMapping(value = "/pAssignment")
	public String showUserPage(Model model) {
		List<ParticipantAssignment> assignments = participantAssignmentService.fetchAll();
		model.addAttribute("assignments", assignments);
		return "assignments_page";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/addAssignment")
	public String addAssignment(@ModelAttribute("assignment") @Valid ParticipantAssignment assignment, BindingResult result, Model model) {
		if (!result.hasErrors()) {
			participantAssignmentService.add(assignment);
		}
		List<ParticipantAssignment> assignments = participantAssignmentService.fetchAll();
		model.addAttribute("assignments", assignments);
		return "assignments_page";
	}

}
