package epam.edu.dao;

import java.util.List;

import epam.edu.bean.Group;

public interface GroupDAO {

	public void create(Group group);

	public void update(Group group);

	public void remove(Long id);

	public Group fetchById(Long id);

	public List<Group> getAll();
}
