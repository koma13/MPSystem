package epam.edu.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import epam.edu.bean.Group;
import epam.edu.dao.GroupDAO;

@Repository
@Transactional
public class GroupDAOImpl implements GroupDAO {
	
	@PersistenceContext
	private EntityManager entityManager;


	public void create(Group group) {
		entityManager.persist(group);
	}
	
	public void update(Group group) {
		entityManager.merge(group);
	}

	public void remove(Long id) {
		Group group = entityManager.find(Group.class, id);
		entityManager.remove(group);

	}

	public List<Group> getAll() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Group> cq = cb.createQuery(Group.class);
		Root<Group> groups = cq.from(Group.class);
		cq.select(groups);
		List<Group> result = entityManager.createQuery(cq).getResultList();
		return result;
	}

	public Group fetchById(Long id) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Group> criteriaQuery = builder.createQuery(Group.class);
		Root<Group> groupRoot = criteriaQuery.from(Group.class);
		criteriaQuery.select(groupRoot);
		criteriaQuery.where(builder.equal(groupRoot.get("id"),id));
		Group group = entityManager.createQuery(criteriaQuery).getSingleResult();
		return group;

	}


}
