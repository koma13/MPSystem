package epam.edu.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import epam.edu.bean.ParticipantAssignment;
import epam.edu.dao.ParticipantAssignmentDAO;

@Repository
@Transactional
public class ParticipantAssignmentDAOImpl implements ParticipantAssignmentDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	public void create(ParticipantAssignment participantAssignment) {
		entityManager.persist(participantAssignment);

	}
	public void update(ParticipantAssignment participantAssignment) {
		entityManager.merge(participantAssignment);
		
	}


	public ParticipantAssignment fetchById(Long id) {
		String qlString = "SELECT p FROM " + ParticipantAssignment.class.getName() + " p WHERE p.id = ?1";
		TypedQuery<ParticipantAssignment> query = entityManager.createQuery(qlString, ParticipantAssignment.class);
		query.setParameter(1, id);
		return query.getSingleResult();
	}

	public List<ParticipantAssignment> getAll() {
		TypedQuery<ParticipantAssignment> query = entityManager.createQuery("select p from " + ParticipantAssignment.class.getName() + " p", ParticipantAssignment.class);
		return query.getResultList();
	}


	public void deleteById(Long id) {
		ParticipantAssignment participantAssignment = entityManager.find(ParticipantAssignment.class, id);
		entityManager.remove(participantAssignment);
		
	}

}
