package epam.edu.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import epam.edu.bean.Group.STATUS;
import epam.edu.dao.StatisticDAO;

@Repository
@Transactional
public class StatisticDAOImpl implements StatisticDAO {

	@PersistenceContext
	private EntityManager entityManager;

	/*
	 * SELECT name, email, level FROM mp.employee inner join mp.group_mp on
	 * employee.id = group_mp.mentor where mp.group_mp.status = 'IN_PROGRESS'
	 * group by mentor having count(mp.group_mp.mentor)>2
	 */
	public List<Object[]> statistic1() {
		STATUS in_progress = STATUS.IN_PROGRESS;
		Query query = entityManager.createQuery("SELECT e.name, e.email, e.level, g.status  FROM Employee e join e.groupsMentor g where g.status=:in_progress group by e.id having count(g.mentor)>2")
				.setParameter("in_progress", in_progress);
		List<Object[]> results = query.getResultList();
		return results;

	}

	/*
	 * SELECT * FROM mp.group_mp inner join mp.employee on employee.id =
	 * group_mp.mentee inner join participant_assignment on
	 * participant_assignment.group_id = group_mp.id inner join mp_assignments
	 * on mp_assignments.programs_id = participant_assignment.id inner join
	 * mentorship_program on mentorship_program.id = mp_assignments.programs_id
	 * where group_mp.mentor is NULL and mentorship_program.office_location =?
	 */
	public List<Object[]> statistic2(String location) {
		Query query = entityManager
				.createQuery(
						"SELECT e.name, g.mentor, mp.officeLocation FROM Employee e join e.groupsMentor g join g.assisgnment ga join ga.program mp where g.mentor is NULL and mp.officeLocation=:location")
				.setParameter("location", location);
		List<Object[]> results = query.getResultList();
		return results;
	}

	/*
	 * SELECT employee.id, name, email, level, group_mp.mentor,
	 * DATEDIFF(group_mp.planned_end, group_mp.planned_start)/7 AS weeks from
	 * employee inner join group_mp on employee.id = group_mp.mentor where
	 * mentor is not null group by mentor order by weeks desc
	 */
	public List<Object[]> statistic3() {
		String query = "SELECT distinct e.name, e.email, e.level, g.mentor.id, DATEDIFF(g.plannedEnd, g.plannedStart)/7 AS weeks "
				+ "FROM Employee e join e.groupsMentor g where g.mentor is not null group by g.mentor.id order by weeks desc";
		List<Object[]> results = entityManager.createQuery(query).getResultList();
		return results;
	}

	/*
	 * select office_location, count(*) as mp_programs from mentorship_program
	 * group by office_location
	 */
	public List<Object[]> statistic4() {
		List<Object[]> results = entityManager.createQuery("SELECT officeLocation, count(mp)  FROM MentorshipProgram mp group by officeLocation").getResultList();
		return results;
	}

	// SELECT * FROM mp.group_mp where status = 'FINISHED' and planned_end<
	// Date()
	public List<Object[]> statistic5(Date date) {
		STATUS in_progress = STATUS.FINISHED;
		Query query = entityManager.createQuery("SELECT distinct e.name, e.email, g.status  FROM Employee e join e.groupsMentor g where g.status=:in_progress and g.plannedEnd<:date")
				.setParameter("in_progress", in_progress).setParameter("date", date);
		List<Object[]> results = query.getResultList();
		return results;
	}

}
