package epam.edu.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import epam.edu.bean.Lecture;
import epam.edu.bean.Employee;
import epam.edu.dao.LectureDAO;

@Repository
@Transactional
public class LectureDAOImpl implements LectureDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public void create(Lecture lecture) {
		entityManager.persist(lecture);
	}

	public void update(Lecture lecture) {
		entityManager.merge(lecture);

	}

	public void remove(Long id) {
		Employee employee = entityManager.find(Employee.class, id);
		entityManager.remove(employee);

	}

	public Lecture fetchById(Long id) {
		String qlString = "SELECT p FROM " + Lecture.class.getName() + " p WHERE p.id = ?1";
		TypedQuery<Lecture> query = entityManager.createQuery(qlString, Lecture.class);
		query.setParameter(1, id);
		return query.getSingleResult();
	}

	public List<Lecture> fetchAll() {
		TypedQuery<Lecture> query = entityManager.createQuery("select l from " + Lecture.class.getName() + " l", Lecture.class);
		return query.getResultList();
	}

}
