package epam.edu.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import epam.edu.bean.MentorshipProgram;
import epam.edu.dao.MentorshipProgramDAO;

@Repository
@Transactional
public class MentorshipProgramDAOImpl implements MentorshipProgramDAO {
	
	@PersistenceContext
	private EntityManager entityManager;


	public void create(MentorshipProgram mentorshipProgram) {
		entityManager.persist(mentorshipProgram);

	}

	
	public void update(MentorshipProgram mentorshipProgram) {
		entityManager.merge(mentorshipProgram);
		
	}
	public void deleteById(Long id) {
		MentorshipProgram mentorshipProgram = entityManager.find(MentorshipProgram.class, id);
		entityManager.remove(mentorshipProgram);
		
	}


	public List<MentorshipProgram> getAll() {
		TypedQuery<MentorshipProgram> query = entityManager.createQuery("select m from " + MentorshipProgram.class.getName() + " m", MentorshipProgram.class);
		return query.getResultList();
	}




	public MentorshipProgram fetchById(Long id) {
		String qlString = "SELECT p FROM " + MentorshipProgram.class.getName() + " m WHERE m.id = ?1";
		TypedQuery<MentorshipProgram> query = entityManager.createQuery(qlString, MentorshipProgram.class);
		query.setParameter(1, id);
		return query.getSingleResult();
	}


	
}
