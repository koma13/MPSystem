package epam.edu.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import epam.edu.bean.Employee;
import epam.edu.dao.EmployeeDAO;

@Repository("employeeDAO")
@Transactional
public class EmployeeDAOImpl implements EmployeeDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public void create(Employee employee) {
		entityManager.persist(employee);
	}

	public void update(Employee employee) {
		entityManager.merge(employee);

	}

	public void deleteById(Long id) {
		Employee employee = entityManager.find(Employee.class, id);
		entityManager.remove(employee);

	}

	public Employee fetchById(Long id) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Employee> criteriaQuery = builder.createQuery(Employee.class);
		Root<Employee> employeeRoot = criteriaQuery.from(Employee.class);
		criteriaQuery.select(employeeRoot);
		criteriaQuery.where(builder.equal(employeeRoot.get("id"),id));
		Employee employee = entityManager.createQuery(criteriaQuery).getSingleResult();
		return employee;

	}

	public List<Employee> getAll() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
		Root<Employee> employees = cq.from(Employee.class);
		cq.select(employees);
		List<Employee> result = entityManager.createQuery(cq).getResultList();
		return result;
	}

}
