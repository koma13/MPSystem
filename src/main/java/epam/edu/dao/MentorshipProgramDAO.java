package epam.edu.dao;

import java.util.List;

import epam.edu.bean.MentorshipProgram;

public interface MentorshipProgramDAO {

	public void create(MentorshipProgram mentorshipProgram);

	public void update(MentorshipProgram mentorshipProgram);

	public void deleteById(Long id);

	public MentorshipProgram fetchById(Long id);

	public List<MentorshipProgram> getAll();

}
