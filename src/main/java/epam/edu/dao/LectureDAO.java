package epam.edu.dao;

import java.util.List;

import epam.edu.bean.Lecture;

public interface LectureDAO {
	
	public void create(Lecture lecture);
	
	public void update(Lecture lecture);

	public void remove(Long id);

	public Lecture fetchById(Long id);

	public List<Lecture> fetchAll();
}
