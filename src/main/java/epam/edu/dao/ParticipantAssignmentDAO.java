package epam.edu.dao;

import java.util.List;

import epam.edu.bean.ParticipantAssignment;

public interface ParticipantAssignmentDAO {

	public void create(ParticipantAssignment participantAssignment);

	public void update(ParticipantAssignment participantAssignment);

	public void deleteById(Long id);

	public ParticipantAssignment fetchById(Long id);

	public List<ParticipantAssignment> getAll();

}
