package epam.edu.dao;

import java.util.Date;
import java.util.List;

public interface StatisticDAO {

	public List<Object[]> statistic1();

	public List<Object[]> statistic2(String location);

	public List<Object[]> statistic3();

	public List<Object[]> statistic4();

	public List<Object[]>  statistic5(Date date);
}
