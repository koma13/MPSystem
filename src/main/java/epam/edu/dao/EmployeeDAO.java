package epam.edu.dao;

import java.util.List;

import epam.edu.bean.Employee;

public interface EmployeeDAO {

	public Employee fetchById(Long id);

	public List<Employee> getAll();

	public void deleteById(Long id);

	public void create(Employee employee);

	public void update(Employee employee);

}
