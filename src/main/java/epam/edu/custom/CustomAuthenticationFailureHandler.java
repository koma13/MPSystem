package epam.edu.custom;

import java.io.IOException;
import java.sql.Date;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import epam.edu.bean.LoginDetails;
import epam.edu.messaging.MessageSender;

public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Autowired
	MessageSender messageSender;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
		String username = request.getParameter("username");
		Date date = new Date(Calendar.getInstance().getTime().getTime());

		LoginDetails loginDetails = new LoginDetails(username, false, "Invalid username or/and password!", date, request.getRemoteAddr());
		if (loginDetails.getUserName().equals("admin")) {
			messageSender.sendLoginDetailsMessage(loginDetails);
		} else {
			messageSender.sendLoginDetailsMessage(loginDetails);
		}
		response.sendRedirect(request.getContextPath() + "/loginError");
	}
}
