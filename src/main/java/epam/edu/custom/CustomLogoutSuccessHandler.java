package epam.edu.custom;

import java.io.IOException;
import java.sql.Date;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import epam.edu.bean.LoginDetails;
import epam.edu.messaging.MessageSender;

public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {

	@Autowired
	MessageSender messageSender;

	public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
		if (authentication != null && authentication.getDetails() != null) {
			try {
				LoginDetails loginDetails = new LoginDetails(authentication.getName(), true, "Logged out successfuly", new Date(Calendar.getInstance().getTime().getTime()), httpServletRequest.getRemoteAddr());
				if (loginDetails.getUserName().equals("admin")) {
					messageSender.sendAdminLoginDetailsMessage(loginDetails);
				} else {
					messageSender.sendLoginDetailsMessage(loginDetails);
					httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/login");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}