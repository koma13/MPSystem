package epam.edu.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

public class DBUtils {

	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void initialize() {

		DataSource dataSource = getDataSource();
		
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			statement.executeUpdate("INSERT INTO employee" + "(name, email, level, manager, primary_skill, birthdate) VALUES" + "('name1','email1@co.co', 'L4', 'manager1', 'JAVA', '1900-01-01')");
			statement.executeUpdate("INSERT INTO employee" + "(name, email, level, manager, primary_skill, birthdate) VALUES" + "('name2','email2@co.c', 'L3', 'manager2', 'HTML', '1900-01-01')");
			statement.executeUpdate("INSERT INTO employee" + "(name, email, level, manager, primary_skill, birthdate) VALUES" + "('name3','email3@co.c', 'L4', 'manager3', 'JAVA', '1900-01-01')");
			statement.executeUpdate("INSERT INTO employee" + "(name, email, level, manager, primary_skill, birthdate) VALUES" + "('name4','email4@co.c', 'L2', 'manager4', 'JS', '1900-01-01')");
			statement.executeUpdate("INSERT INTO employee" + "(name, email, level, manager, primary_skill, birthdate) VALUES" + "('name5','email5@co.c', 'L1', 'manager5', 'JAVA', '1900-01-01')");
			statement.executeUpdate("INSERT INTO employee" + "(name, email, level, manager, primary_skill, birthdate) VALUES" + "('name6','email6@co.c', 'L1', 'manager6', 'HTML', '1900-01-01')");
			statement.executeUpdate("INSERT INTO employee" + "(name, email, level, manager, primary_skill, birthdate) VALUES" + "('name7','email7@co.c', 'L4', 'manager7', 'JAVA', '1900-01-01')");
			statement.executeUpdate("INSERT INTO employee" + "(name, email, level, manager, primary_skill, birthdate) VALUES" + "('name8','email8@co.c', 'L1', 'manager1', 'JS', '1900-01-01')");
			
			statement.executeUpdate("INSERT INTO mentorship_program" + "(name, start_date, end_date, office_location) VALUES" + "('program_name1','2016-01-07', '2016-11-07', 'Lviv')");
			statement.executeUpdate("INSERT INTO mentorship_program" + "(name, start_date, end_date, office_location) VALUES" + "('program_name2','2016-02-07', '2016-11-07', 'Lviv')");
			statement.executeUpdate("INSERT INTO mentorship_program" + "(name, start_date, end_date, office_location) VALUES" + "('program_name3','2016-03-07', '2016-11-14', 'Lviv')");
			statement.executeUpdate("INSERT INTO mentorship_program" + "(name, start_date, end_date, office_location) VALUES" + "('program_name4','2016-03-07', '2016-12-07', 'Kharkiv')");
			statement.executeUpdate("INSERT INTO mentorship_program" + "(name, start_date, end_date, office_location) VALUES" + "('program_name5','2016-04-07', '2016-12-27', 'Lviv')");
			statement.executeUpdate("INSERT INTO mentorship_program" + "(name, start_date, end_date, office_location) VALUES" + "('program_name6','2016-05-07', '2016-11-07', 'Kyiv')");
			statement.executeUpdate("INSERT INTO mentorship_program" + "(name, start_date, end_date, office_location) VALUES" + "('program_name7','2016-12-07', '2016-12-07', 'Lviv')");

			statement.executeUpdate("INSERT INTO group_mp" + "(mentor, mentee, planned_start, actual_start, planned_end, status) VALUES" + "(1, 5, '2016-03-07', '2016-03-10', '2016-05-07', 'IN_PROGRESS')");
			statement.executeUpdate("INSERT INTO group_mp" + "(mentor, mentee, planned_start, actual_start, planned_end, status) VALUES" + "(2, 6, '2016-03-07', '2016-03-10', '2016-10-07', 'FINISHED')");
			statement.executeUpdate("INSERT INTO group_mp" + "(mentor, mentee, planned_start, actual_start, planned_end, status) VALUES" + "(1, 5, '2016-03-07', '2016-12-10', '2016-12-07', 'IN_PROGRESS')");
			statement.executeUpdate("INSERT INTO group_mp" + "(mentor, mentee, planned_start, actual_start, planned_end, status) VALUES" + "(1, 8, '2016-03-07', '2016-03-10', '2016-12-07', 'IN_PROGRESS')");
			statement.executeUpdate("INSERT INTO group_mp" + "(mentor, mentee, planned_start, actual_start, planned_end, status) VALUES" + "(null, 6, '2016-03-07', '2016-03-10', '2016-08-07', 'CANCELLED')");
			statement.executeUpdate("INSERT INTO group_mp" + "(mentor, mentee, planned_start, actual_start, planned_end, status) VALUES" + "(null, 6, '2016-03-07', '2016-12-10', '2016-12-07', 'IN_PROGRESS')");
			statement.executeUpdate("INSERT INTO group_mp" + "(mentor, mentee, planned_start, actual_start, planned_end, status) VALUES" + "(4, 8, '2016-03-07', '2016-03-10', '2016-12-07', 'IN_PROGRESS')");
			statement.executeUpdate("INSERT INTO group_mp" + "(mentor, mentee, planned_start, actual_start, planned_end, status) VALUES" + "(1, 7, '2016-03-07', '2016-11-10', '2016-10-07', 'CANCELLED')");
			
			statement.executeUpdate("INSERT INTO participant_assignment" + "(role, status, group_id, program_id) VALUES" + "('MENTOR','APPROVED_RM', 1, 1)");
			statement.executeUpdate("INSERT INTO participant_assignment" + "(role, status, group_id, program_id) VALUES" + "('MENTEE','APPROVED_RM', 2, 2)");
			statement.executeUpdate("INSERT INTO participant_assignment" + "(role, status, group_id, program_id) VALUES" + "('MENTOR','CONFIRMED_CDP',3, 3)");
			statement.executeUpdate("INSERT INTO participant_assignment" + "(role, status, group_id, program_id) VALUES" + "('MENTEE','APPROVED_RM', 4, 4)");
			statement.executeUpdate("INSERT INTO participant_assignment" + "(role, status, group_id, program_id) VALUES" + "('MENTEE','CONFIRMED_CDP', 5, 5)");
			statement.executeUpdate("INSERT INTO participant_assignment" + "(role, status, group_id, program_id) VALUES" + "('MENTEE','CONFIRMED_CDP', 7, 6)");

			statement.executeUpdate("INSERT INTO lecture" + "(domain_area, topic, lector, duration_min, scheduled_time, status, mentorship_program_id) VALUES" + "('Java core','Concurrency', 'lector1', 90,'2016-03-07 09:00:00', 'SCHEDULED', 1 )");
			statement.executeUpdate("INSERT INTO lecture" + "(domain_area, topic, lector, duration_min, scheduled_time, status, mentorship_program_id) VALUES" + "('Java core','Design patterns', 'lector1', 60,'2016-03-07 09:00:00', 'SCHEDULED', 2 )");
			statement.executeUpdate("INSERT INTO lecture" + "(domain_area, topic, lector, duration_min, status, mentorship_program_id) VALUES" + "('DB','MongoDB', 'lector1', 90, 'ON_PREPARATION', 3 )");
			statement.executeUpdate("INSERT INTO lecture" + "(domain_area, topic, lector, duration_min, status, mentorship_program_id) VALUES" + "('Spring','Spring Boot', 'lector1', 120, 'ON_PREPARATION', 2 )");

		
		
			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}