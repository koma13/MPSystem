package epam.edu.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import epam.edu.bean.Lecture;

@Deprecated
public class LectureRowMapper implements RowMapper<Lecture> {

	public Lecture mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		Lecture lecture = new Lecture();
		lecture.setId(resultSet.getLong(1));
		lecture.setDomainArea(resultSet.getString(2));
		lecture.setTopic(resultSet.getString(3));
		lecture.setLector(resultSet.getString(4));
		lecture.setDurationMin(resultSet.getInt(5));
		lecture.setScheduledTime(resultSet.getDate(6));
		return lecture;
	}
}