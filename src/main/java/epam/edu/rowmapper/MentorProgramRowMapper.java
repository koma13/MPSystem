package epam.edu.rowmapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import epam.edu.bean.MentorshipProgram;

@Deprecated
public class MentorProgramRowMapper implements RowMapper<MentorshipProgram> {

	public MentorshipProgram mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		MentorshipProgram program = new MentorshipProgram();
		program.setId(resultSet.getLong(1));
		program.setName(resultSet.getString(2));			
		program.setOfficeLocation(resultSet.getString(3));
		program.setStartDate(resultSet.getDate(4));
		program.setEndDate(resultSet.getDate(5));
		return program;
	}
}