package epam.edu.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import epam.edu.bean.ParticipantAssignment;

@Deprecated
public class PAssignmentRowMapper implements RowMapper<ParticipantAssignment> {

	public ParticipantAssignment mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		ParticipantAssignment assignment = new ParticipantAssignment();
		assignment.setId(resultSet.getLong(1));
		//assignment.setemployee(resultSet.getString(2));
		assignment.setStatus(resultSet.getString(3));
		assignment.setRole(resultSet.getString(4));

		return assignment;
	}
}