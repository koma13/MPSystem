package epam.edu.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import epam.edu.bean.Employee;
import epam.edu.bean.Employee.LEVEL;
import epam.edu.bean.Employee.PRIMARY_SKILL;

@Deprecated
public class EmployeeRowMapper implements RowMapper<Employee> {

	public Employee mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		
		Employee employee = new Employee();
		employee.setId(resultSet.getLong(1));
		employee.setName(resultSet.getString(2));
		employee.setEmail(resultSet.getString(3));
		employee.setBirthdate(resultSet.getDate(4));
		employee.setSkill(PRIMARY_SKILL.valueOf(resultSet.getString(6)));
		employee.setLevel(LEVEL.valueOf(resultSet.getString(7)));
		employee.setDateCreated(resultSet.getDate(8));
		employee.setCreatedBy(resultSet.getString(9));
		employee.setDateModified(resultSet.getDate(10));
		employee.setModifiedBy(resultSet.getString(11));
		return employee;
	}
}