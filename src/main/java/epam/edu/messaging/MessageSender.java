package epam.edu.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import epam.edu.bean.ActivityDetails;
import epam.edu.bean.LoginDetails;

@Component
public class MessageSender {

	@Autowired
	private JmsTemplate jmsTemplate;

	public void sendAdminLoginDetailsMessage(final LoginDetails loginDetails) {
		jmsTemplate.setPriority(10);
		sendLoginDetailsMessage(loginDetails);
	}

	public void sendLoginDetailsMessage(final LoginDetails loginDetails) {
		System.setProperty("org.apache.activemq.SERIALIZABLE_PACKAGES", "domain");
		jmsTemplate.setDefaultDestinationName("login-queue");

		jmsTemplate.send(new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				ObjectMessage objectMessage = session.createObjectMessage(loginDetails);
				return objectMessage;
			}
		});
	}

	public void sendActivityInfoMessage(final ActivityDetails activityDetails) {
		jmsTemplate.setDefaultDestinationName("activity-queue");
		jmsTemplate.send(new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				ObjectMessage objectMessage = session.createObjectMessage(activityDetails);
				return objectMessage;
			}
		});
	}
}