package epam.edu.service;

import java.util.List;

import epam.edu.bean.MentorshipProgram;

public interface MentorshipProgramService {

	List<MentorshipProgram> fetchPrograms();

	void addProgram(MentorshipProgram program);

}
