package epam.edu.service;

public interface LoginDetailsQueueService {

	public void addLoginDetailsQueue(String loginMsg, boolean isSuccess, String userIP);
	
	public void addLoginDetailsQueue(String userName, String loginMsg, boolean isSuccess, String userIP);
}
