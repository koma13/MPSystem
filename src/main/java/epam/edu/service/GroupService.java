package epam.edu.service;

import java.util.List;

import epam.edu.bean.Group;

public interface GroupService {

	List<Group> fetchAll();

	void addAll(Group group);

}
