package epam.edu.service;

import java.util.List;

import epam.edu.bean.Employee;

public interface EmployeeService {

	List<Employee> fetchemployees();

	void add(Employee employee);

	void update(Employee employee);

	void delete(Long id);

	Employee fetchById(Long id);

}
