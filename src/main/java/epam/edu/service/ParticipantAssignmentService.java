package epam.edu.service;

import java.util.List;

import epam.edu.bean.ParticipantAssignment;

public interface ParticipantAssignmentService {

	List<ParticipantAssignment> fetchAll();

	void add(ParticipantAssignment assignment);

}
