package epam.edu.service;

import java.util.List;

import epam.edu.bean.Lecture;

public interface LectureService {

	List<Lecture> fetchAll();

	void add(Lecture lecture);

}
