package epam.edu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epam.edu.bean.Group;
import epam.edu.dao.GroupDAO;
import epam.edu.service.GroupService;

@Service
public class GroupServiceImpl implements GroupService{
	
	@Autowired
	private GroupDAO groupDao;

	public List<Group> fetchAll() {
		return groupDao.getAll();
	}
	public void addAll(Group group) {
		groupDao.create(group);
	}

}
