package epam.edu.service.impl;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epam.edu.bean.ActivityDetails;
import epam.edu.messaging.MessageSender;
import epam.edu.service.ActivityDetailsQueueService;

@Service
public class ActivityDetailsQueueServiceImpl implements ActivityDetailsQueueService{
	
	@Autowired
	MessageSender messageSender;

	public void addActivityDetailsQueue(String ip, String loginMsg) {
		ActivityDetails activityDetails = new ActivityDetails(ip, loginMsg, new Date(Calendar.getInstance().getTime().getTime()));
		messageSender.sendActivityInfoMessage(activityDetails);
	}

}
