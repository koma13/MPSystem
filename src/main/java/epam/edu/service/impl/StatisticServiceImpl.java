package epam.edu.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epam.edu.dao.StatisticDAO;
import epam.edu.service.StatisticService;

@Service
public class StatisticServiceImpl implements StatisticService {

	@Autowired
	private StatisticDAO statisticDAO;

	public List<Object[]> statistic1() {
		return statisticDAO.statistic1();
	}

	public List<Object[]> statistic2(String location) {
		return statisticDAO.statistic2(location);
	}

	public List<Object[]> statistic3() {
		return statisticDAO.statistic3();
	}

	public List<Object[]> statistic4() {
		return statisticDAO.statistic4();
	}

	public List<Object[]> statistic5(Date date) {
		return statisticDAO.statistic5(date);
	}

}
