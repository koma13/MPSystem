package epam.edu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epam.edu.bean.Lecture;
import epam.edu.dao.LectureDAO;
import epam.edu.service.LectureService;

@Service
public class LectureServiceImpl implements LectureService{
	
	@Autowired
	private LectureDAO lectureDao;

	public List<Lecture> fetchAll() {
		return lectureDao.fetchAll();
	}
	public void addAll(Lecture lecture) {
		lectureDao.create(lecture);
	}
	public void add(Lecture lecture) {
		lectureDao.create(lecture);
		
	}

}
