package epam.edu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epam.edu.bean.Employee;
import epam.edu.dao.EmployeeDAO;
import epam.edu.service.EmployeeService;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDAO employeeDAO;

	public List<Employee> fetchemployees() {
		return employeeDAO.getAll();
	}
	
	public void update(Employee employee) {
		employeeDAO.update(employee);
	}

	public void delete(Long id) {
		employeeDAO.deleteById(id);
	}

	public Employee fetchById(Long id) {
		return employeeDAO.fetchById(id);
	}

	public void add(Employee employee) {
		employeeDAO.create(employee);
		
	}

}
