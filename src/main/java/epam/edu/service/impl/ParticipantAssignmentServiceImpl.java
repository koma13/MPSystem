package epam.edu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epam.edu.bean.ParticipantAssignment;
import epam.edu.dao.ParticipantAssignmentDAO;
import epam.edu.service.ParticipantAssignmentService;

@Service
public class ParticipantAssignmentServiceImpl implements ParticipantAssignmentService {

	@Autowired
	private ParticipantAssignmentDAO participantAssignmentDAO;

	public List<ParticipantAssignment> fetchAll() {
		return participantAssignmentDAO.getAll();
	}

	public void add(ParticipantAssignment assignment) {
		participantAssignmentDAO.create(assignment);
	}
}
