package epam.edu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epam.edu.bean.MentorshipProgram;
import epam.edu.dao.MentorshipProgramDAO;
import epam.edu.service.MentorshipProgramService;

@Service
public class MentorshipProgramServiceImpl implements MentorshipProgramService {

	@Autowired
	private MentorshipProgramDAO mentorshipProgramDAO;

	public List<MentorshipProgram> fetchPrograms() {
		return mentorshipProgramDAO.getAll();
	}

	public void addProgram(MentorshipProgram program) {
		mentorshipProgramDAO.create(program);
	}
}
