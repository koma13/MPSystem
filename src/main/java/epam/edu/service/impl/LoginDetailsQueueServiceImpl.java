package epam.edu.service.impl;

import java.sql.Date;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import epam.edu.bean.LoginDetails;
import epam.edu.messaging.MessageSender;
import epam.edu.service.LoginDetailsQueueService;

@Service
public class LoginDetailsQueueServiceImpl implements  LoginDetailsQueueService{
	
	@Autowired
	MessageSender messageSender;
	

	public void addLoginDetailsQueue(String loginMsg, boolean isSuccess, String userIP) {
		String userName = getUserName();
		addLoginDetailsQueue(userName, loginMsg, isSuccess, userIP);
	}

	public void addLoginDetailsQueue(String userName, String loginMsg, boolean isSuccess, String userIP) {
		LoginDetails loginDetails = new LoginDetails(userName, isSuccess, loginMsg, new Date(Calendar.getInstance().getTime().getTime()), userIP);
		System.out.println(loginDetails);
		if (loginDetails.getUserName().equals("admin")) {
			messageSender.sendAdminLoginDetailsMessage(loginDetails);
		} else {
			messageSender.sendLoginDetailsMessage(loginDetails);
		}

	}

	private String getUserName() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return auth.getName();
	}


}
