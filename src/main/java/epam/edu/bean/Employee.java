package epam.edu.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NotEmpty(message = "Please enter your name")
	private String name;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yy-mm-dd")
	private Date birthdate;
	@Email
	private String email;
	@Enumerated(EnumType.STRING)
	@Column(name = "primary_skill")
	private PRIMARY_SKILL skill;
	@Enumerated(EnumType.STRING)
	private LEVEL level;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created")
	private Date dateCreated;
	@Column(name = "created_by")
	private String createdBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modified")
	private Date dateModified;
	@Column(name = "modified_by")
	private String modifiedBy;
	private String manager;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "mentor")
	private Set<Group> groupsMentor;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "mentee")
	private Set<Group> groupsMentee;

	public enum LEVEL {
		L1, L2, L3, L4, L5
	};

	public enum PRIMARY_SKILL {
		JAVA, JS, HTML

	};

	public Set<Group> getGroupsMentor() {
		return groupsMentor;
	}

	public void setGroupsMentor(Set<Group> groupsMentor) {
		this.groupsMentor = groupsMentor;
	}

	public Set<Group> getGroupsMentee() {
		return groupsMentee;
	}

	public void setGroupsMentee(Set<Group> groupsMentee) {
		this.groupsMentee = groupsMentee;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public LEVEL getLevel() {
		return level;
	}

	public void setLevel(LEVEL level) {
		this.level = level;
	}

	public PRIMARY_SKILL getSkill() {
		return skill;
	}

	public void setSkill(PRIMARY_SKILL skill) {
		this.skill = skill;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Employee() {
		super();
	}

	public Employee(Long id, String name, Date birthdate, String email, String manager, PRIMARY_SKILL skill, LEVEL level, Date dateCreated, String createdBy, Date dateModified, String modifiedBy) {
		super();
		this.id = id;
		this.name = name;
		this.birthdate = birthdate;
		this.email = email;
		this.manager = manager;
		this.skill = skill;
		this.level = level;
		this.dateCreated = dateCreated;
		this.createdBy = createdBy;
		this.dateModified = dateModified;
		this.modifiedBy = modifiedBy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthdate == null) ? 0 : birthdate.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((manager == null) ? 0 : manager.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (birthdate == null) {
			if (other.birthdate != null)
				return false;
		} else if (!birthdate.equals(other.birthdate))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (manager == null) {
			if (other.manager != null)
				return false;
		} else if (!manager.equals(other.manager))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "employee [id=" + id + ", name=" + name + ", birthdate=" + birthdate + ", email=" + email + ", manager=" + manager + ", level=" + level + ", skill=" + skill + ", dateCreated="
				+ dateCreated + ", createdBy=" + createdBy + ", dateModified=" + dateModified + ", modifiedBy=" + modifiedBy + "]";
	}

	public String toStringShort() {
		return "employee [id=" + id + ", name=" + name + ", birthdate=" + birthdate + ", email=" + email + ", manager=" + manager + ", level=" + level + ", skill=" + skill + "]";
	}

}
