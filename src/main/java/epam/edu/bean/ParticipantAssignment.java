package epam.edu.bean;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "participant_assignment")
public class ParticipantAssignment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Enumerated(EnumType.STRING)
	private ROLE role;
	@Enumerated(EnumType.STRING)
	private STATUS status;

	@ManyToOne
	@JoinColumn(name = "program_id", nullable = true, insertable = false, updatable = false)
	private MentorshipProgram program;
	

	@OneToOne
	@JoinColumn(name = "group_id")
	private Group group;

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public MentorshipProgram getProgram() {
		return program;
	}

	public void setProgram(MentorshipProgram program) {
		this.program = program;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRole(ROLE role) {
		this.role = role;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public enum ROLE {
		MENTOR, MENTEE, CURATOR, LECTOR
	};

	public enum STATUS {
		PROPOSED, APPROVED_RM, CONFIRMED_CDP, ON_HOLD
	};

	public String getRole() {
		return role.toString();
	}

	public void setRole(String role) {
		this.role = ROLE.valueOf(role);
	}

	public String getStatus() {
		return status.toString();
	}

	public void setStatus(String status) {
		this.status = STATUS.valueOf(status);
	}

	public ParticipantAssignment() {
		super();
	}

	public ParticipantAssignment(Employee employee, ROLE role, STATUS status) {
		super();
		this.role = role;
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
