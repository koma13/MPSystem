package epam.edu.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "group_mp")
public class Group implements Serializable {

	private static final long serialVersionUID = 4576100461039514091L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "mentor", nullable = true, insertable = false, updatable = false)
	private Employee mentor;
	@ManyToOne
	@JoinColumn(name = "mentee", insertable = false, updatable = false)
	private Employee mentee;
	@Column(name = "planned_start")
	@Temporal(TemporalType.DATE)
	private Date plannedStart;
	@Column(name = "planned_end")
	@Temporal(TemporalType.DATE)
	private Date plannedEnd;
	@Column(name = "actual_start")
	@Temporal(TemporalType.DATE)
	private Date actualStart;
	@Column(name = "actual_end")
	@Temporal(TemporalType.DATE)
	private Date actualEnd;
	@Enumerated(EnumType.STRING)
	private STATUS status;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "group")
	private ParticipantAssignment assisgnment;

	public ParticipantAssignment getAssisgnments() {
		return assisgnment;
	}

	public void setAssisgnments(ParticipantAssignment assisgnments) {
		this.assisgnment = assisgnments;
	}

	public enum STATUS {
		INITIATION, IN_PROGRESS, FINISHED, CANCELLED
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Employee getMentor() {
		return mentor;
	}

	public void setMentor(Employee mentor) {
		this.mentor = mentor;
	}

	public Employee getMentee() {
		return mentee;
	}

	public void setMentee(Employee mentee) {
		this.mentee = mentee;
	}

	public Date getPlannedStart() {
		return plannedStart;
	}

	public void setPlannedStart(Date plannedStart) {
		this.plannedStart = plannedStart;
	}

	public Date getPlannedEnd() {
		return plannedEnd;
	}

	public void setPlannedEnd(Date plannedEnd) {
		this.plannedEnd = plannedEnd;
	}

	public Date getActualStart() {
		return actualStart;
	}

	public void setActualStart(Date actualStart) {
		this.actualStart = actualStart;
	}

	public Date getActualEnd() {
		return actualEnd;
	}

	public void setActualEnd(Date actualEnd) {
		this.actualEnd = actualEnd;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Group [id=" + id + ", mentor=" + mentor + ", mentee=" + mentee + ", plannedStart=" + plannedStart + ", plannedEnd=" + plannedEnd + ", actualStart=" + actualStart + ", actualEnd="
				+ actualEnd + ", status=" + status + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actualEnd == null) ? 0 : actualEnd.hashCode());
		result = prime * result + ((actualStart == null) ? 0 : actualStart.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((mentee == null) ? 0 : mentee.hashCode());
		result = prime * result + ((mentor == null) ? 0 : mentor.hashCode());
		result = prime * result + ((plannedEnd == null) ? 0 : plannedEnd.hashCode());
		result = prime * result + ((plannedStart == null) ? 0 : plannedStart.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (actualEnd == null) {
			if (other.actualEnd != null)
				return false;
		} else if (!actualEnd.equals(other.actualEnd))
			return false;
		if (actualStart == null) {
			if (other.actualStart != null)
				return false;
		} else if (!actualStart.equals(other.actualStart))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (mentee == null) {
			if (other.mentee != null)
				return false;
		} else if (!mentee.equals(other.mentee))
			return false;
		if (mentor == null) {
			if (other.mentor != null)
				return false;
		} else if (!mentor.equals(other.mentor))
			return false;
		if (plannedEnd == null) {
			if (other.plannedEnd != null)
				return false;
		} else if (!plannedEnd.equals(other.plannedEnd))
			return false;
		if (plannedStart == null) {
			if (other.plannedStart != null)
				return false;
		} else if (!plannedStart.equals(other.plannedStart))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

}
