package epam.edu.bean;

import java.io.Serializable;
import java.sql.Date;

public class LoginDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userName;

	private boolean isSuccess;

	private String message;

	private Date date;
	
	private String userIP;

	public String getUserIP() {
		return userIP;
	}

	public void setUserIP(String userIP) {
		this.userIP = userIP;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}



	public LoginDetails(String userName, boolean isSuccess, String message, Date date, String userIP) {
		super();
		this.userName = userName;
		this.isSuccess = isSuccess;
		this.message = message;
		this.date = date;
		this.userIP = userIP;
	}

	public LoginDetails() {

	}

	@Override
	public String toString() {
		return "LoginDetails [userName=" + userName + ", isSuccess=" + isSuccess + ", message=" + message + ", date=" + date + ", userIP=" + userIP + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoginDetails other = (LoginDetails) obj;
		if (message != other.message)
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

}
