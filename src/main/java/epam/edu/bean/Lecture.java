package epam.edu.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lecture")
public class Lecture {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "domain_area")
	private String domainArea;
	private String topic;
	private String lector;
	@Column(name = "duration_min")
	private Integer durationMin;
	@Column(name = "scheduled_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date scheduledTime;
	@Enumerated(EnumType.STRING)
	private STATUS status;
	
	@ManyToOne
	@JoinColumn(name = "mentorship_program_id")
	private MentorshipProgram mentorshipProgram;

	public MentorshipProgram getMentorshipProgram() {
		return mentorshipProgram;
	}

	public void setMentorshipProgram(MentorshipProgram mentorshipProgram) {
		this.mentorshipProgram = mentorshipProgram;
	}

	public enum STATUS {
		ON_PREPARATION, SCHEDULED, PASSED
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDomainArea() {
		return domainArea;
	}

	public void setDomainArea(String domainArea) {
		this.domainArea = domainArea;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getLector() {
		return lector;
	}

	public void setLector(String lector) {
		this.lector = lector;
	}

	public Integer getDurationMin() {
		return durationMin;
	}

	public void setDurationMin(Integer durationMin) {
		this.durationMin = durationMin;
	}

	public Date getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(Date scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	public Lecture() {
		super();
	}

	public Lecture(String domainArea, String topic, String lector, Date scheduledTime, Integer durationMin) {
		super();
		this.domainArea = domainArea;
		this.topic = topic;
		this.lector = lector;
		this.durationMin = durationMin;
		this.scheduledTime = scheduledTime;
	}

	public STATUS getStatus() {
		return status;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lecture other = (Lecture) obj;
		if (domainArea == null) {
			if (other.domainArea != null)
				return false;
		} else if (!domainArea.equals(other.domainArea))
			return false;
		if (durationMin == null) {
			if (other.durationMin != null)
				return false;
		} else if (!durationMin.equals(other.durationMin))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lector == null) {
			if (other.lector != null)
				return false;
		} else if (!lector.equals(other.lector))
			return false;
		if (scheduledTime == null) {
			if (other.scheduledTime != null)
				return false;
		} else if (!scheduledTime.equals(other.scheduledTime))
			return false;
		if (status != other.status)
			return false;
		if (topic == null) {
			if (other.topic != null)
				return false;
		} else if (!topic.equals(other.topic))
			return false;
		return true;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((domainArea == null) ? 0 : domainArea.hashCode());
		result = prime * result + ((durationMin == null) ? 0 : durationMin.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lector == null) ? 0 : lector.hashCode());
		result = prime * result + ((scheduledTime == null) ? 0 : scheduledTime.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((topic == null) ? 0 : topic.hashCode());
		return result;
	}

	
}
