package epam.edu.bean;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "mentorship_program")
public class MentorshipProgram {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "program_id")
	private Long id;
	private String name;
	@Column(name = "office_location")
	private String officeLocation;
	@Column(name = "start_date")
	@Temporal(TemporalType.DATE)
	private Date startDate;
	@Column(name = "end_date")
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "program")
	private Set<ParticipantAssignment> assignments;

	public Set<ParticipantAssignment> getAssignments() {
		return assignments;
	}


	@OneToMany(mappedBy = "mentorshipProgram", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Lecture> lecture;

	public void setAssignments(Set<ParticipantAssignment> assignments) {
		this.assignments = assignments;
	}

	public Set<Lecture> getLecture() {
		return lecture;
	}

	public void setLecture(Set<Lecture> lecture) {
		this.lecture = lecture;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOfficeLocation() {
		return officeLocation;
	}

	public void setOfficeLocation(String officeLocation) {
		this.officeLocation = officeLocation;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public MentorshipProgram() {
		super();
		lecture = new HashSet<Lecture>();
	}

	public MentorshipProgram(String name, String officeLocation, Date startDate, Date endDate) {
		super();
		this.name = name;
		this.officeLocation = officeLocation;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((officeLocation == null) ? 0 : officeLocation.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MentorshipProgram other = (MentorshipProgram) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (officeLocation == null) {
			if (other.officeLocation != null)
				return false;
		} else if (!officeLocation.equals(other.officeLocation))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}

}
