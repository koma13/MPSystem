package epam.edu.bean;

import java.io.Serializable;
import java.util.Date;

public class ActivityDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userIP;

	private String activityDetails;

	private Date date;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getUserIP() {
		return userIP;
	}

	public void setUserIP(String userIP) {
		this.userIP = userIP;
	}

	public String getIsSuccessMsg() {
		return activityDetails;
	}

	public void setIsSuccessMsg(String isSuccessMsg) {
		this.activityDetails = isSuccessMsg;
	}

	public ActivityDetails() {
	}

	public ActivityDetails(String userIP, String isSuccessMsg, Date date) {
		super();
		this.userIP = userIP;
		this.activityDetails = isSuccessMsg;
		this.date = date;
	}

	@Override
	public String toString() {
		return "ActivityDetails [userName=" + userIP + ", activityDetails=" + activityDetails + ", date=" + date + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((activityDetails == null) ? 0 : activityDetails.hashCode());
		result = prime * result + ((userIP == null) ? 0 : userIP.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActivityDetails other = (ActivityDetails) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (activityDetails == null) {
			if (other.activityDetails != null)
				return false;
		} else if (!activityDetails.equals(other.activityDetails))
			return false;
		if (userIP == null) {
			if (other.userIP != null)
				return false;
		} else if (!userIP.equals(other.userIP))
			return false;
		return true;
	}

}
