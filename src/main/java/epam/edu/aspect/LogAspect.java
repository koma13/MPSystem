package epam.edu.aspect;

import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;

import epam.edu.bean.Employee;
import epam.edu.service.EmployeeService;

@Aspect
public class LogAspect {

	Logger logger = Logger.getLogger(LogAspect.class);

	@Autowired
	private EmployeeService employeeService;

	@Around(value = "execution(* epam.edu.controller.*.save(..))")
	public Object logAdd(ProceedingJoinPoint joinPoint) throws Throwable {
		Object[] args = joinPoint.getArgs();
		HttpServletRequest request = (HttpServletRequest) args[2];
		for (Object item : Arrays.asList(joinPoint.getArgs())) {
			if (item.getClass().equals(Employee.class)) {
				Employee employee = (Employee) item;
					employee.setDateCreated(new Date());
					employee.setCreatedBy(request.getRemoteAddr());
				}

		}
		return joinPoint.proceed();
	}
	
	@Around(value = "execution(* epam.edu.controller.*.update(..))")
	public Object logUpdate(ProceedingJoinPoint joinPoint) throws Throwable {
		Object[] args = joinPoint.getArgs();
		HttpServletRequest request = (HttpServletRequest) args[3];
		for (Object item : Arrays.asList(joinPoint.getArgs())) {
			if (item.getClass().equals(Employee.class)) {
				Employee employee = (Employee) item;
				employee.setDateModified(new Date());
				employee.setModifiedBy(request.getRemoteAddr());

			}
		}
		return joinPoint.proceed(args);
	}

	@After("execution(* epam.edu.service.*.*(..))")
	public void allMethodCallLogger(JoinPoint joinPoint) {
		Signature signature = joinPoint.getSignature();
		logger.info("Call method: " + signature);
	}

	@AfterThrowing(pointcut = "execution(* epam.edu.service.*.*(..))", throwing = "e")
	public void errorInterceptor(JoinPoint joinPoint, Throwable e) {
		Signature signature = joinPoint.getSignature();
		String methodName = signature.getName();
		String stuff = signature.toString();
		String arguments = Arrays.toString(joinPoint.getArgs());
		logger.info("We have caught exception in method: " + methodName + " with arguments " + arguments + "\nand the full toString: " + stuff + "\nthe exception is: " + e.getMessage(), e);
	}

}
